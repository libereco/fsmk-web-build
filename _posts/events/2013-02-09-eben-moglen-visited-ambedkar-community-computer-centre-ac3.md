---
layout: post
title: "Eben Moglen visited Ambedkar Community Computer Centre (AC3)"
date: 2013-02-09 18:46:03
category: events
---

<p align="JUSTIFY">
          Eben Moglen, came to Bangalore, with a packed itinerary, which included lectures at the top educational institutions in the country and also a program organized by Kannada Bloggers. He was presented with a rare gift on his arrival - a book penned by the children of AC3 based on their personal experiences at the center. The book was titled "The Future is Ours". Eben Moglen considered the gift as truly special, primarily because the children, from the most marginalized community in India belong to the class of society towards whom his sympathies, as a premier leftist, would flow.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  It was this book which prompted Eben to express his desire to meet the children in AC3. As planned, he visited the centre on December 14 at 11 am, accompanied by Mishy, member of SFLC (Software Freedom Law Centre) and Dr Nagarjuna, Chairman of FSF-India. Members of Ambedkar Youth
</p>

<p align="JUSTIFY">
  Sangha, kids of AC3, volunteers of AID and Volunteers of Free Software – Karnataka were present for the meeting.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  Eben was also impressed my Mani's paintings and the book 'The Future is Ours' compiled by Sarasu. He attended the presentation taken by Sarasu about the centre. Even in the absence of Santhosh, the lead singer, the children sang a song about creating a world without violence and warfare. Santhosh,has gone to assist his mother at the household where she works as a housemaid in order to earn their livelihood. Eben was so impressed with the book that he asked Sarasu and Mani, for their autographs. Mishy and Nagarjuna too followed suit and also collected autographs from Mani and Sarasu. Eben even went on to say that this was the most important event in his visit to Bangalore so far.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  Then there was a discussion, involving the guests and all the people associated with AC3 and the children. It was on open forum, where suggestions were mooted towards bringing in more innovations to the centre. Discussions were held as to whether a more serious approach based on present syllabi was needed or whether the current approach which encouraged learning in a playful way was to be retained.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  Eben suggested creating an environment that is free from the fear of examinations and about giving children the freedom to explore and come up with innovations and new ideas. There were also discussions on improving the amenities at the center. Eben gave more creative ideas for the betterment of the center and to deal with difficulties better.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  The nature of the center's work and future plans where discussed at length. The key point in the discussions was how to make this as a movement in order to bring the marginalized class to the mainstream of the society. Eben expressed his willingness to help the center financially and also promised that he would leverage his influence for providing other facilities needed for development.There were also discussions on what could be done in order to transfer the AC3 into a center of excellence, so that it can serve as a role model for other similar institutions. When the students were asked their views on the same, Sarasu promptly opined that they had already attained a minimum level of computer awareness, and that efforts must go towards helping other underprivileged children to attain the same level of knowledge; the selflessness of the children striking a chord in the minds of the participants.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  Towards the end, Haridas, a daily wage laborer and an active member of the Ambedkar Youth Sangha, and Santosh, joined the meeting. Haridas suggested ways to bring out the latent talents in children. The heartening aspect was that everyone, including the children, was actively part of the discussion. The concept of the Free Society was in full view.
</p>

<p align="JUSTIFY">
   
</p>

<p align="JUSTIFY">
  The meeting of people from diverse backgrounds irrespective of their caste, race, creed and nationality portrayed how Free Software was able to transcend all man made boundaries. The discussion ended on an optimistic note, with Eben congratulating all the teachers, students and volunteers and exhorted them to carry the baton forward, thereby fueling the struggle to help building a better world for all to live in.
</p>
