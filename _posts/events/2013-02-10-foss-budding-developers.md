---
layout: post
title: "FOSS for Budding Developers"
date: 2013-02-10 14:34:18
category: events
---

Free Software Movement-Karnataka (FSMK) a member organisation FREE SOFTWARE MOVEMENT INDIA in association with IBM Software Labs is organizing the encore workshop on 'FOSS for budding developers' at CMR Institute of Technology, Bangalore, on 10th and 11th April 2010, targeted at students becoming code contributors - foss developers. Please visit http://nc2010.fsmk.org/LDW/. You can also contact myself or Mr.Prabodh lecturer CMRIT college over email or phone for more details of the conference.

Contact Persons: Naveen Mudunuru(+919986403928), Prabodh (9844205442).

**Session Details**

Inaugral Address :  Prof. Gopinath, IISc, Vice President, FSMI

**Day1**

1.  Eclipse (Software Development Environment related)
2.  Linux Kernel workshop
3.  OpenPegasus (System Management Development related)

**Day2**

1.  Linux Test Project (Test development/testing for Linux OS related)
2.  Linux Kernel workshop
3.  Apache Geronimo (Web Server/Web Applications development related)
4.  Guest Talk: L Subramani, Chief Technical Editor, Deccan Herald

Note: Each Day consists of 3 parallel sessions as mentioned above. Students are free to choose any one of these sessions on either day

**Prerequisites**  
Its expected that all participants will have:

1.  Basic knowledge of Operating Systems
2.  Good working knowledge of C
3.  Hands-on experience desirable
4.  Kernel programming experience is a bonus
5.  Basic scripting knowledge (ex: shell scripts)
6.  Working knowledge of GNU/Linux.
7.  Booting a GNU/Linux System
8.  Basic system administration (adding/removing users etc)
9.  Basic commands (ls, ps, chmod, mkdir, vi, etc)

**Event Details**

Date: 10th and 11th April 2010.  
Venue: CMR Institute of Technology  
No. 132, AECS Layout, IT Park Road,  
Bengaluru - 560 037.

 
