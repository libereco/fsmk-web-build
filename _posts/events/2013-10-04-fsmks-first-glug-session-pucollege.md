---
layout: post
title: "FSMK's First GLUG session in P.U.College"
date: 2013-10-04 13:20:34
category: events
---

Spreading the Free Software Ideologies in the schools(including p.u.c) for the first time.The first experiment was made in our college SGH (Shrimathi Gangamma Hombegowda)P.U.College. This is an aided institute. Before handling the session itself, we had four volunteers from this college . Who were the regular students of Amigo Community Centre one of the GLUG of FSMK.

The students namely Divya(Myself),Leelavathi,Rukhaya from Science Department & Arvind from Commerce Department.The session was conducted on 15th of June 2013 Saturday.More than 120 students from different Departments like Science,Commerce & Arts studing Second P.U,participated in the session.This session was handled by our FSMK members Karthik Bhat,Jeeva & Hari Prasadh. The session was mainly based on giving the introduction about Free Software , FSMK, introduction to some of the Free Software applications like: Blender, K-turtle, and so on.And also ,there were some videos screened for entertainment related to Free Software.After the session, the volunteers were asked to list down the students names ,who are all interested to use &learn Free Software ideologies and also for making GLUG in the College.About 24 students gave their name and contact number.

This activity was done with the help of Rameez's idea.  Further meetings with the students will be made by the volunteers of SGH College GLUG.And,we will fix a date for the installation of Free Software in our college Computer lab.

We hope that,this would create a major change ,like not only in P.U.College, this will surely spread in our own college's divisions like Degree and High School.

DSCN6036

-Divya
