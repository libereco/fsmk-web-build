---
layout: post
title: "Google Summer of Code(GSoC), Ruby SoC, KDE SoC, Outreach Program for Women, ..."
date: 2015-03-13 20:22:01
category: events
---

The Free Software Community is looking for you eagerly, are you interested to work for the community?  

Various attempts are being made by the Free Software Community to reach out to more and more people and encourage them to contribute to the community. Not only is the community actively doing this, they are also investing heavily on it. This Sunday join us in interacting with many Ex participants of all these various attempts.  
Aruna who got selected for OPW and contributed to GNOME documentation.  
<a href="https://www.facebook.com/pshastry" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;">Pallavi Shastry</a> who got selected for RSoC and contributed features to Diaspora  
<a href="https://www.facebook.com/tirtha.p.chatterjee" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;">Tirtha Chatterjee</a>, GSoCer for 2011 and 2012 and contributed to KDE  
<a href="https://www.facebook.com/sathyam.vellal" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;">Sathyam Vellal</a> GSoCer 2013 worked on <a href="http://boost.org/" rel="nofollow nofollow" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;" target="_blank">Boost.org.</a>  
<a href="https://www.facebook.com/rtnpro" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;">Ratnadeep Debnath</a> Mentor GSoC 2014 on Fedora-Waartaa.  
<a href="https://www.facebook.com/synpro" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;">Sayan Chowdhury</a> GsoCer 2013 worked on Fedora-DarkServer.

Sunday, March 9, 2014at 9:30am - 4:30pm in UTC+02  
@Free Software Movement Karnataka No. 121/17, 1st Floor, 6th main, 14th Cross, Wilson garden, Bengaluru - 560030
