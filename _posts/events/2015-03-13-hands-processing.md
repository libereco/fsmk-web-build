---
layout: post
title: "Hands on with 'Processing'"
date: 2015-03-13 20:37:48
category: events
---

Building interactive programs, hands on, with Processing, a java-based  
language designed for beginners.  

Pre-requisites:  

Download Processing from <a href="http://processing.org/download/?processing" rel="nofollow nofollow" style="color: rgb(59, 89, 152); cursor: pointer; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;" target="_blank">http://processing.org/download/?processing</a>  
for the machine of your choice. I will also have all the download  
packages, so it is okay if this step is not done, provided students  
arrive early, so I can install the package on their machines.

Sunday, December 29, 2013at 11:00am - 1:00pm  
@FSMK Office
