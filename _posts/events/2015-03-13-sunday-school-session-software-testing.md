---
layout: post
title: "Sunday School Session on Software Testing"
date: 2015-03-13 20:20:10
category: events
---

Topics !   

1. What are your instincts towards life ? yes it matters !  
2. What does the word 'TEST' mean to you ?   
3. What does it take to be a Software Tester ?  
4. Life Cycle   
5. Techniques   
6. Test Cases on White, Grey and Black Box   
7. Screw/Break/Crash on application   
8. Effective Defect Logging techniques   
9. Open Source Testers   
10. Money ...!!!  

Note : Participants try to bring their laptop

Sunday, April 6, 2014at 2:00pm - 4:00pm  
@FSMK Office Bangalore, India
