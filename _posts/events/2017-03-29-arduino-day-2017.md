---
title: Arduino Day 2017
category: events
layout: post
date: 2017-03-29 00:09:00 +0530
startdate:  2017-04-01 00:09:00 +0530
enddate: 2017-04-01 00:17:00 +0530
discourse_topic_id: 571
---

Free Software Movement Karnataka (Freedom Hardware) will be celebrating "Arduino Day 2017" on coming April 1st, Saturday.
Arduino Day is a worldwide birthday celebration of Arduino– organized directly by the community– where people interested in Arduino or any interesting ideas get together, share their experiences, and learn more.

Arduino Day is open to anyone who wants to celebrate Arduino and all the amazing things that have been done (or can be done!) with the open-source platform. The events will offer different types of activities, tailored to local audiences all over the world

All user groups, makerspaces, hackerspaces, fablabs, associations, teachers, professionals, and newbies are welcome. You can attend the event, It doesn’t matter whether you are a, student, Maker, an engineer, a designer, a developer or an educator

Meetup - FSMK Office <http://www.openstreetmap.org/node/247095388611>

Arduino day agenda will include

1. Open House Day
2. Show and Tell
3. Workshop
4. Panel/Discussion
5. Demos

We look forward your collaborations and in different ways

1. School Students, UG & PG students,Academicians or any who wish to know about the stuffs and Arduino Community can come and have a look into demos
2. Anyone with a some idea or prototype and can participate in show and tell.
3. Even if you don't know anything about Arduino... You are welcome, you will take out something from our workshops that day. (For registrations send your details to itsanup25@gmail.com)
4. Anyone wishes to present Talks or Demos do let us know. We will include.

Workshop is from 2-pm to 3pm on arduino basics.

Others can come and leave at anytime.

For more details
<https://etherpad.fsmk.org/p/Arduino_Day_FSMK_Agenda>
