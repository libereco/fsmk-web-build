---
title: From Karshapanas to Crypto- A Brief History of Money
category: events
layout: post
date: 2017-04-18 00:34:00 +0530
startdate:  2017-04-23 14:30:00 +0530
enddate: 2017-04-23 17:00:00 +0530
discourse_topic_id: 741
---

Developments such as demonetisation and crypto-currencies have led people to scratch their heads about the nature of money - and its relation to the economy. This talk traces the history and the logic of money from ancient times (like the Mauryan ‘karshapanas’) to the modern crypto-currencies (such as Bitcoin).

Speaker - Secki P Jose
<br>
Date - 23rd April 2017
<br>
Time -02:30 PM
<br>
Venue - [FSMK Office](http://www.openstreetmap.org/node/2470953886)
