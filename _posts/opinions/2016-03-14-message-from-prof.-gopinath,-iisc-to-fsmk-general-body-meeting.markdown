---
layout: post
title:  "Message from Prof. Gopinath, IISc to FSMK General Body Meeting"
date:   2015-03-14 11:10 +0530
category: opinions
---

Dear FSMK GB Members

While free software movement has caught on in the "backend", it has not yet caught on that much in the "frontend" or desktop. The former is due to mostly the "efficiency" argument but it also has opened an opportunity for the latter. However, even in developed markets, it is proving to be a difficult problem for free software to establish itself on the desktop. Probably, free software on the  conventional desktop is no longer an important issue. Has it become possibly irrelevant? Or has it changed? I believe that instead of the "desktop", the "mobiletop" has become important. What can FSMK do to make situation with respect to mobiletop more conducive to free software?

First, it may have to come up with customisable software that can be used in small businesses. Or more simply, find software that does most of the work and repackage it for use in different fields. For eg, there is an openMRS system (for a medical record system). It may be made to run in a client server model such as with the "SANA"  mobile client from MIT. Can we think of other areas where such an approach is useful? In Bangalore, it may be for transportation. Or even, attempt an oversight system for monitoring public projects (for eg, metro) on an "automated" basis based on newspaper reports? Can we use the energies released by the "anti-corruption" movement to get some hackers to work with us? Can we also get small businesses get started on a sound basis to carry some of this work?

Secondly, we may have to work on creating an ecosystem for Indian languages on the "mobiletop". Even if there may not be an economic incentive right now (given the dominance of English), it is still worth attempting this so that the linguistic and cultural strengths of non-metropolitan centres does not get devalued even faster than what is happening right now. Free software, I believe, has an important role here.

Thirdly, we may have to find a way of making free software relevant in the education sector,especially at the school level. We have a "School OS" already. What can we do make it more accessible? Can we think of a mobile client that helps manage the homework and assignments? There are packages like this on the desktop but putting it on a mobile can make it widely usable.

I am aware that each of the above is non-trivial but progress, even if not dramatic, can have significant dividends.  Can we use the BE students looking for a project usefully in these endeavours? We need to organise ourselves systematically to make any of these possible. There is energy and talent in plenty, I believe. Can FSMK put them together to make it possible? I do not have any answers but you have experience of organising large events such as the NCFS2010, NCAR2010 or the state level convention in March. I believe you can pull it off!

Best Wishes
Gopi
