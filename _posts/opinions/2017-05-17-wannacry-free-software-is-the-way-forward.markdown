---
title: WannaCry - FREE SOFTWARE is the way forward
date: 2017-05-12 08:02:00 +0530
layout: post
pinned: true
category: opinions
---

The world is still in shock from the recent WannaCry attack. Though experts claim the situation is under control, they don't rule out the possibilities of more such attacks in coming days. As per the reports, WannaCry ransomware  affected approximately 230,000 computers in 150 countries bringing regular operation to a halt in many places. The most affected are American shipping companies and healthcare systems in the UK. The impact of this attack is comparatively low in India.

What is ransomware?

Ransomware is a malware or a malicious program that encrypts the files in computers or smartphones and makes it unusable, while the attackers demand money for decrypting the files. In the recent attack, which is considered to be one of the largest ransomware attacks, crackers were demanding 300$ to 500$ equivalent in bitcoin currency for decrypting the files.

What is wannaCry ransomware?

WannaCry is the name of ransomware that targets the Microsoft Windows operating system. This malware was used to launch the WannaCry ransomware attack on Friday, 12 May 2017. Sometimes it uses different names like WannaCrypt, WannaCry, WanaCrypt0r, WCrypt, WCRY etc. Crackers used the loophole present in Microsoft SMB Protocol to spread this program to other computers. All machines running the version of Windows operating system before Windows 10 without MS17 -010 security patch are prone to this ransomware attack.

Immediate measures to block such attacks in future?

1) Update the security patch MS17-010 released by Microsoft at the earliest.
2) Block port numbers 39, 445, 3389 in your firewall
3) Avoid clicking unknown links while using the Internet
4) Avoid opening email attachments from unknown contacts
5) Disable SMB in windows operating system
6) Set pop-up blocker in your browser
7) Keep your data backup regularly and store in the cloud environment.

The long term solution?

Fixing a problem permanently lies on how better we understand the root cause of the problem. It is not possible to build a completely foolproof program or an operating system that is not prone to any sort of such attacks. But a program and its source code under public scrutiny can identify the loopholes in the system and patch immediately before attackers exploit that vulnerability. This is the main reason all major malware attacks are mainly affecting proprietary software like Microsoft Windows operating systems. When a user does not have any control over the program that he/she is running, the program controls the user. That also makes it is easy for someone else to take complete control over that computer or smartphone. In the case of  Free Software, users control the program, and the source code is open for everyone to access;  this in turns makes  Free Software  more secure when compared to any other proprietary software.

Unlike in the past, nowadays Free Software operating systems (GNU/Linux ) are user-friendly and easy to use. Ubuntu, Mint, Debian Fedora are some widely used PC operating systems with an active community support. By taking a decision to migrate from proprietary operating system to a GNU/Linux operating systems, you are not only staying safe but also become part of culture and community who believe in sharing and collaborating.

The NSA's role here?

The malware is spread on the network by leveraging the vulnerability in   "Server Message Block" (SMB), a network protocol in the Windows Operating System. Last month it was revealed that the software  "EternalBlue" is developed by the American National Security Agency (NSA) and has been used to leak personal information using this security vulnerability. WannaCry is believed to be using the same software. The unholy alliance between the American IT giants and the National Security Agency is no more a secret.  Edward Snowden, a national security contractor, was one of the first persons to disclose this illicit relationship between NSA and American IT giants which revealed the massive surveillance program targeting the citizens with the direct help of American IT giants.Hence, any long-term solutions need to be discussed and developed outside the ambit of the unholy alliance between NSA and IT giants,  the Free Software way.
