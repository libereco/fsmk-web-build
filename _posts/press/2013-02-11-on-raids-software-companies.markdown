---
layout: post
title:  "On the raids by Software Companies"
date:   2013-11-02 17:43:01 +0530
category: press
---







	Free Software Movement of Karnataka, has released a press note expressing regret on the Software raids conducted on the small scale vendors/users in the name of violation of copy right laws.

FSMK appeals to small scale vendors who are dependent on these soft wares for their livelihood to not panic, as there are a number of "Free Software" alternatives for the proprietary software being used.

FSMK assures that its hundreds of free software volunteers would assist and train software users to switch over to Free software almost at free of cost and distribute Copy-left Free software.

FSMK also appeals to government that Free software should be promoted to avoid these Copyright hassles forced upon Small Scale Vendors.
