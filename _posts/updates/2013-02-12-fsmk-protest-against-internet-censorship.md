---
layout: post
title: "FSMK Protest Against Internet Censorship"
date: 2013-02-12 18:21:04
category: updates
---

Freedom of expression takes a new dimension altogether when placed in the context of the digital channels like blogs, social networks or microblogs in the Internet. While there is no absolutism associated with any freedom, the applicability or the non applicability of restrictions and laws between the real world and the digital space needs clear understanding and a distinction based on this understanding.

The stand of Free Software Movement India, has always been to sustain and perpetuate the freedom of computer users and information technology benefactors.

The Government of India today has proposed and implementing severe, unsubstantiated endorsement of censorship, leading to the curbing of fundamental rights of expression in the Internet, FSMI reiterates its stand to Free Software supporters by presenting the the threat this move would pose to the essence of neutrality in the Internet and democracy in the country.

**Campaigns**

FSMI is of the understanding that without increasing awareness amongst the netizens on the issue at hand, and limited activism of the activists alone cannot create any substantial impact. To make the Government give heed to the decries of freedom in the Internet a mass appeal must have to be made.  
Further, collaborating with other related teams to drive this campaign forward was consciously taken up.

**Signing petition**

To garner support of online activists and users with concern an online petition by the change.org and few thousand signatures have been assimilated with their opinions.

https://www.change.org/petitions/mps-of-india-support-the-annulment-motion-to-protect-internet-freedom-stopitrules


**Wikispace**

Bringing together the “Friends of Internet” was the motto of this campaign and as mentioned before, FSMI lent out their support and sought support various organisations concerned about the curbing of freedom in the internet. Posters, pamphlets and write ups were churned up in this attempt and consolidated on a wikispace for general public to view on the Internet.

http://friendsofinternet.wikispaces.com/

**Sessions and Talks**

With a strong foothold amongst students, regional teams of FSMI have been elucidating students about the issues with formal talks, informal discussions to debate the various facets of the issue at hand, and the role as internet users, these students could play.

A video of one of the Free Software teams in Bangalore under FSMK made an opinion collage showing the dissent of students against internet censorship.

http://doubteverything.org/sites/default/files/glug.ogv

**Protest**

The Karnataka team, FSMK culminated the campaigns by organising a street protest with slogan shouting for grabbing media attention at the Town Hall, Bangalore. About hundred students and internet users from various sections participated in the protest and it received wide spread media coverage.

**Result**

From a state where very little cognizance or even awareness of the issue of curbing freedom in the Internet, the efforts by teams like FSMI were able to create a commotion in the Rajya Sabha with support from few Members of the Parliament.

The motion to annul the rules that allow internet censorship, moved by Mr. P. Rajeeve, came up for discussion in the Rajya Sabha (on the 17th of May).  The Parliamentarians have listened to our demand to support the motion and there was an overwhelming support across party lines.

The parliament debated the issue for close to two hours.  Mr. Kapil Sibal, Minister for Communications & Information technology assured the house that a meeting of the Members as well as the industry and all the stakeholders will be convened and that the Government would implement any consensus that emerges out of such a meeting.

Though the Rajya Sabha did not pass the motion, the discussion marks an important milestone.  It has resulted in the Government convening the meeting to seek consensus.

**Excerpts from coverage and activist voices**

**Harshita, Student Engineering**

"Now, imagine you found out something valuable ,some scandal ,fraud, and you want to let the world know. Web becomes your best platform,may be you blog about it. But before the world gets to hear about it, it was lost!!Your post had been removed.The suppression is almost real.  
This day is not far once the new IT rules are passed in the parliament"

"A mass of young ,energetic and vibrant youth , software engineers  and bloggers voiced their opinion . Being among them I felt the power of connectivity, an air of pride, of struggle for freedom."


http://footprintsthroughmymind.blogspot.in/2012/04/censored.html

\---|\---|\---|-

**Ashfaq ,  robotics programmer, teacher, student and blogger - photo blog**


http://sneakpeakintomyworld.blogspot.in/2012/04/theinternettransforming-society-and.html

   
\---|\---|\---|\---|--

**Raghavendra,  electronics teacher-lecturer**

"A true people's democracy today is not feasible without sustaining Internet democracy".

http://beta.bodhicommons.org/article/globalising-opinions-and-counter-currents-the-internet-story


\---|\---|\---|\---|\---|-

**A first time protester, student, anonymous**

http://fossphosis.blogspot.in/2012/04/world-inside-your-head.html

" I was losing my weakness of feeling self-conscious in the middle of strangers and I was enjoying it. The candles in the end added solemnity and as I walked back home, under the eagles flying so high, I promised myself that I would protest again, perhaps with more conviction. That it would mean more, much more the next time. "


\---|\---|\---|\---|\---|--

**Pictures**

https://www.facebook.com/media/set/?set=a.3812036783416.167940.1352421967&type=1&l=18f7cc662b#!/photo.php?fbid=3812039183476&set=a.3812036783416.167940.1352421967&type=3&l=18f7cc662b&theater


\---|\---|\---|  
   
**Press**  
   
\---|\---|\---|  
TV  
   
http://indiatoday.intoday.in/video/techies-protest-against-censoring-social-media/1/185572.html  
   
**Print**  
\---|\---|\---|  
   
BANGALORE: Protests against government's alleged attempt to govern the internet is gathering steam, with a public interest litigation in Kerala, a signature campaign and mass protests in Karnataka besides the political left throwing its weight behinds demand to withdraw the recently amended laws.  
   
http://economictimes.indiatimes.com/tech/internet/anti-internet-censorship-protests-gather-steam/articleshow/12830871.cms  
   
IT hub sets the tone for internet freedom movement  
http://articles.timesofindia.indiatimes.com/2012-04-22/bangalore/31381872\_1\_intermediaries-guidelines-internet-freedom-content  
   
It's a regular day at the Parappana Agrahara prison in Bangalore. The media has arrived to interview high profile criminals held there. Among politicians accused of land grabbing and corruption, and industrialists, is a young boy. When asked what he had done to land up there, he says: “I blogged.” The media and other “high-profile” criminals scoff at him for not being “criminal enough”.  
   
http://www.thehindu.com/todays-paper/tp-national/tp-karnataka/article3341220.ece

 
