---
layout: post
title: "FSMK Newsletter Volume 07"
date: 2013-04-08 18:29:05
category: updates
---

We are bringing up some issues we think are important in the Indian context. Through the editorial, we are discussing the issue of Digital Divide. We feel this topic is rarely discussed amongst free software groups, though we live in a Indian situation.The plight of the last man in India and his digital connection. Mostly our discussions have been around 'freedom', 'choice'. As P Sainath says "if people had a choice they would have had food".

For the first time we have published a Kannada article as well. Expect more in the coming editions. This is as part of our commitment to take free Software to the Kannada space. There is also a bit of  covering lot of small news in the News section.

Apart from discussion we believe in ground work, so in this effort we need you to carry the flag to newer heights.

<a href="http://fsmk.org//sites/default/files/NewsLetter/FSMK_Issue7.pdf" style="margin: 0px; padding: 0px; text-decoration: initial; color: rgb(38, 140, 108); font-family: Arial, sans-serif, Helvetica; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);" target="_blank">FSMK News Letter Issue 07</a>

 

Regards,  
FSMK EB-Team.
