---
layout: post
title: "FSMK Newsletter Volume 4"
date: 2013-04-08 18:22:12
category: updates
---

FSMK saga continues..

The fourth Newsletter from FSMK is here..

FSMK is greatful to its readers of all walks of likes and life.

We are glad to receive your comments whether it is a criticism or praise..

Write to us and distribute this edition to as much of your contacts as possible.

We welcome you to have your name on one of the articles in the forthcoming editions.

<a href="http://fsmk.org//sites/default/files/NewsLetter/FSMK_Issue4_Sept_2009.pdf" style="margin: 0px; padding: 0px; text-decoration: initial; color: rgb(38, 140, 108); font-family: Arial, sans-serif, Helvetica; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);" target="_blank">FSMK News Letter Issue 04</a>

 

Thanks and Regards,  
FSMK EB Team.

 
