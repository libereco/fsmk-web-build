---
layout: post
title: "VTU Need Not Fall in the M$ Dream$park Trap , Campaign & Protest against M$ VTU deal"
date: 2015-03-07 17:51:22
category: updates
---

Microsoft DreamSpark is a programme from Microsoft primarily aimed at giving software titles under its hood free of charge for students. A student needs to provide a proof of student status for access to these softwares. As a follow-up to the DreamSpark programme, Microsoft is entering a tie-up with VTU to include the softwares that are part of DreamSpark to the VTU curriculum.

Though it seems like a gift to the student community, it serves the interests of Microsoft in the long run as there are many free softwares already available. This is a part of the hidden agenda of Microsoft to challenge the growth of free software development in India. An institution as prestigious as VTU should not Lock-In to a vendor like Microsoft and modify its curriculum based on the interests of Microsoft. The alternative is to include softwares from free software domain, which can serve the same purpose and match or exceed the Microsoft softwares in performances.
