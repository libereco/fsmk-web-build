---
layout: post
title: "Introducing FSMK Discuss"
date: 2015-10-11 19:34:27
category: updates
---

We are glad to announce new forum https://discuss.fsmk.org  
hosted on discourse , a free software for "Civilized Discussion". Discourse is a forum software for improving quality of forum discussion.

Who is it for?  
Anyone interested in free software, digital freedom and society in general.  
What can they find here?

*   People
*   Ideas

Guidelines for participation

*   Do not indulge in personal attacks. Criticize ideas, not people.
*   Search for duplicates before posting.
*   Post in relevant categories for better visibility and participation.
*   Consider posting constructive criticism.
*   Help build the community

Have fun discoursing!
