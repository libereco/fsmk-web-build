---
layout: post
title:  "Reflections from the Annual General Body meeting 2014 : A guiding note"
date:   2015-11-11 13:25 +0530
category: updates
---

Overview

From the formation of a small organising team in 2007 and the official launch of FSMK in 2009 with just a handful of volunteers we have grown quantitatively and qualitatively. We have over twenty active GNU/Linux User Groups(GLUGs) spread over six districts of Karnataka. The quality of the workshops, summer camps, QIP programs, technical sessions and philosophical maturity of the GLUG members has definitely improved. The first few years of FSMK involved a lot of "bootstrapping" to get activities up and running consistently. However, to ensure sustainability, the last EC decided to ensure that democratisation of the GLUGs became a priority so that the organisation would be filled with new life and vigour. This decision has proved to be a good one as the quality of the delegation participating in the recent Annual General Body meeting has definitely improved and will continue to do so.

Attendance

Based on the active membership within each GLUG, a quota was allocated and delegates were elected. Sixteen out of twenty GLUGs sent representatives to the General Body(GB). The attendance was at sixty three. There was also a good representation from the industry, academia and community centre.

Organisation Decentralisation of responsibility

Ten sub-committees were created in the current GB to take forward special initiatives. They are 1. GLUG coordination, 2. Web & Media, 3. projects, 4. academicians, 5. women, 6. Freedom hardware, 7. working professionals, 8. community centre, 9. Sunday school and 10. Lab Manual & STP. Each sub-committee has a dedicated team lead while volunteers can be part of activities across committees with primary responsibility in their main sub-committee. Further, members are encouraged to propose and execute newer ideas that may help build the movement.

Forming district and local committees

FSMK has seen a leap in the increasing members within each GLUG and increase in the number of GLUGs and the difficulty faced by the EC in mentoring the fledgling GLUGs. The idea is to create district and local committees which would have the responsibility of mentoring and seeing to various organisational activities. The EC will have to take up this task on a priority basis to ensure that the growth of the organisation is not hindered due to manpower constraints within the higher committee.

Gender disparity

FSMK is a reflection of the society that it operates in but we are fully aware of what is and what needs to be done to address this lacuna. The issue of gender disparity has been a concern for quite a while and efforts were taken in the previous EC to bridge the gender gap by ensuring that at least two EC members were women. However, that was not sufficient. While a sub-committee for women was created in the current GB, methods ensuring that our mainstream programs are inclusive needs to be addressed on a priority basis. Our aim should be that the next EC should have 33-50% women representatives.

Issue of formal membership

Due to our proactiveness, we do know that the students community consider themselves to be FSMK members and this is a good thing. However, we do not have a clear cut picture of what the membership of the organisation is. A decision to conduct membership within the three chapters was decided. For the present there has been no membership fee for the student and academic chapters though a membership fee of Rs.1000/- will be levied on the members of the industry chapter. A proper membership process will be initiated soon wherein chapter members would have to fill membership forms and renew it on a yearly basis.

Whole timers for FSMK

FSMK currently has a requirement of at least four-five whole timers dedicated to day-to-day activities. This would allow us to take up industry and academic projects to enhance our quality and also provide support to academic institutions who need technical support and students who need internships. This would also allow us to progressively expand to all the districts of Karnataka.

Strengthening academicians and industry chapters

The sub-committees related to working professionals and academia came out with outlines of their milestones and deliverables. Industry professionals have been involved within the several activities of FSMK though the current sub-committee now allows us to define a set of objectives that would allow us to actively engage with the IT industry. Further, there was an urgent need to provide a platform for students who had graduated to contribute back to the movement which had helped them in their academic and professional lives. The academic chapter has been active over the years though not in a formal manner. It has been providing support to the students chapter in all activities. The idea is to consolidate our contacts in the several institutions across Karnataka and build a stronger movement.

Community centre

We as an organisation have focused on bridging the digital divide and this is what makes us unique among the various techno-political groups. Our activities ensure that we are grounded to the realities and constantly engage with them to change them and ourselves in the process. The three community centres help us immensely in this process by giving our future citizens an opportunity to work with the underprivileged and refine themselves by engaging with the problems of poor in the city of Bangalore. In the near future, GLUGs should consider adopting at least one government school and provide mentorship to the students.

Fraternal relations with like-minded organisations

FSMK has maintained an issue based relationship with several organisations across the State. Increasing campaigns related to issues like the Goonda Act, Section 66A, net neutrality, whistle blowing, among others are outcomes of this coordination. We hope to continue and improve our issue based activities with more such organisations.

Sunday school

Creating a team of master trainers who will create more trainers is an important objective and the Sunday school has been a successful experiment. The school will now have to focus on improving the quality of learner engagement by using better pedagogy. The Sunday school sessions have primarily focused on technical issues but in order to build holistic individuals we will also have to include a small component of academic reading within the session itself.

Conclusion

The discussions and action plans proposed by each of the sub-committees during the general body has provided us with feedback on the path we need to take and the tasks we need to accomplish. While our goals are lofty we are definitely grounded and moving in the right direction. If you would like to be a part of the Free Software Movement or contribute to any specific activity please do check the Contact us section on http://www.fsmk.org " System Admins of the World, Unite" -- Julian Assange,in a talk organised by the Chaos Computer Club (CCC) comparing the 21st century workers of digital era with 19th Century workers of industrial era.
