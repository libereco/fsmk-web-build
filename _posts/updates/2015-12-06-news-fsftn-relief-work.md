---
layout: post
title: "News from FSFTN on Relief work"
date: 2015-12-06 16:37:12
category: updates
---

**6th Dec 2015**

Our volunteers collected the relief materials from FSMK , ITEC which was brought safely from Bangalore by Raghuram and Jeeva from FSMK . It was a huge show of strength from Sister organisations and well wishers The relief materials were segregated and one batch of it was sent to North Madras - Basin Bridge and Manali .Where we distributed materials like - Tetra Pack Milk packets , Diapers , Sanitary Pads , ORS sachets , Dress for kids , Bread packets ,candles , Match Boxes .

Rice and other raw materials were given to places where large scale cooking process is going on in Velachery .

Plans are on to prepare Kits for targetted distribution like a placstic bag with - Soap , Mosquito Coil , Biscuits , Sanitary napkins , Candles , Match Boxes .

Picture shows center for physically challenged people where we gave grossary items received today.
