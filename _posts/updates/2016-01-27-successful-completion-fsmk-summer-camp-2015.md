---
layout: post
title: "Successful completion of FSMK summer camp 2015"
date: 2016-01-27 18:51:14
category: updates
---

The 7-day residential industry orientation camp(a.k.a. <a href="http://camp.fsmk.org/">FSMK summer camp 2015</a>) which was held from 20-26 July 2015 at Reva University concluded
successfully.  This is the fourth year that we are running this flagship programme.

We had around 130 participants, 30+ volunteers from around 15 colleges spread across 8 districts of Karnataka. Three simultaneous technical tracks were conducted apart from the numerous sessions on leadership, free software philosophy, gender equality, cultural sessions, entrepreneurship etc

We would like to highlight a few points that we did differently this year: Our primary session facilitators were students who had been participants in previous year camps.  This allowed us to tap the expertise that was developing within the GLUGs and to showcase the strength of our approach in radically changing the quality of the education system.

The training of camp volunteers was more directed and rigorous ensuring that they had a strong orientation of all the topics well in advance. This allowed for effective peer support which aided the sessions to progress smoothly and participant doubts to be handled in real-time.  We hope that most of our session facilitators for future camps will be our student members.

The numbers this year were reduced to around 160 from last year's 200 participants as we wanted to give adequate attention to improving the
quality of our engagement of participants. The effects of this decision are visible from the feedback that we received from the participants and
volunteers. However, we realise that a lot of students missed the opportunity of joining us.

Related posts

http://harikavreddy.blogspot.in/2015/07/i-always-wanted-my-holidays-to-be.html  
http://saky.in/FSMK-Camp-2015/  
https://shalinir15.wordpress.com/2015/08/01/this-is-how-they-learn-share-and-spread-knowledge-fsmk/  
https://akshatha04.wordpress.com/2015/07/29/fsmkcamp15/  
http://www.sathishmun.comule.com/2015/08/07/fsmk-camp-blog15/

<https://www.flickr.com/photos/124841002@N04/sets/72157656550415899/>
