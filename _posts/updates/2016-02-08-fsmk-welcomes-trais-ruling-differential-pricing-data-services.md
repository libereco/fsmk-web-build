---
layout: post
title: "FSMK Welcomes TRAI's ruling on differential pricing for data services"
date: 2016-02-08 18:46:36
category: updates
---

<div>
  Free Software Movement Karnataka welcomes TRAI's <a href="http://trai.gov.in/WriteReadData/PressRealease/Document/Press_Release_No_13%20.pdf">ruling </a>that disallow service providers to offer or charge discriminatory tariff for data service of content being accessed by consumers. Also we use this opportunity to congratulate everyone took part in campaign online as well as offline to keep up the neutrality of the Internet. TRAI deserves a special appreciation for conducting such a unique consultation process where different aspects of the problem were heard and acted appropriately. TRAI has carried out a thorough examination of the issues in the responses to its Consultation paper on the subject and has finally taken a decision that upholds net neutrality. It has also made clear that it considers net neutrality to be the guiding principle of the internet.
</div>

<div>
  It is clearly a people's victory over corporate propaganda. Crores of money was spent by corporate during the campaignto tear apart the Internet. Media sources say that Facebook might have spent more than 100 crores of money exclusively for "free basics" campaign. Amidst all these ad campaign and PRs we focused  mainly on bringing awareness among our masses through online and offline campaign with the available resources which definitely contributed to raise a pro netneutrality public opinion.  This consultation was a testimony to people's intellectagainst the corporate money based maneuver.
</div>
