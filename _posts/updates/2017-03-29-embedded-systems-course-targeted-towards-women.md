---
title: Embedded Systems Course Targeted Towards Women
category: updates
layout: post
pinned: true
date: 2017-03-29 19:02:00 +0530
discourse_topic_id: 572
---

Greetings all,
We want to conduct a focused program to teach practical embedded systems to women. I have been involved in a similar attempt (without the focus on the free aspect) last year. The website that we made is embeddedforher.com and the learning from that attempt is documented
<https://medium.com/@prithvirajnarendra/embedded-for-her-da465c83e6e8>

Here is a working draft of the program plan
<https://etherpad.fsmk.org/p/free-hw-program>

One of the important tasks to be done is the website for the program. Tanvi Bhakta and Rahul Kondi have graciously volunteered to build it. I feel that a website structure similar to embeddedforher.com would do. I hacked it together somehow and the source is available here
<https://github.com/embedded-for-her>
So, Tanvi and Rahul, can you please start working on creating a website and I'll start working on its content?

Feedback and assistance in any form is welcome. Cheers.
