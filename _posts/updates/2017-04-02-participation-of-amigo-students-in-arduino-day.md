---
title: Amigo Students participated in Arduino Day
category: updates
layout: post
date: 2017-04-01 00:09:00 +0530
discourse_topic_id: 585
---

On April 1st, there was Arduino Day Event at FSMK office. There were three demos which were exhibited. Around 10 students + 3 volunteers of Amigo Community Center participated in this event. The students were very much interested in Open Hardware activities. We have planned continue training up students in Hardware related things in further classes with the help of Anup.
