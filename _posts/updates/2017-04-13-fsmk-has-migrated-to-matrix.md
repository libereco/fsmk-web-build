---
title: FSMK has migrated to Matrix
layout: post
date: 2017-04-13 18:33:00 +0530
category: updates
pinned: true
discourse_topic_id: 734
---

**TL;DR:** We now use Matrix as the primary IM platform. Download Riot and join #fsmk-discuss:matrix.org ASAP!

---

Over the past few years, we have had various discussions on suitable messaging platforms for FSMK. Accordingly, we have made multiple attempts to migrate to platforms that are secure and fully free.

Our last attempt was to use a self-hosted XMPP server and we saw mixed results. The XMPP client of choice, Conversations, had some usability issues that made it hard for some users. The lack of OMEMO support on common desktop clients also made it hard for desktop and multi-device users.

In our latest effort, we have recently switched to Matrix[1]. This post details the current setup and its rationale; and explains the process of joining the new platform.

Choice of platform
------------------

We evaluated many free platforms and concluded that the only feasible one was Matrix. Here are some of the factors that helped us make this decision -

- Matrix is an open standard
- It is federated by design
- Fully free clients and servers are available
- End-to-end encryption is supported (although it is currently beta quality)
- It has all the features we need and more
- It has been tested by us and other groups successfully

We feel very confident about Matrix and its future. We're also happy that if we ever need to move away from it, the open nature of the platform will help us do it in a seamless manner.

About Matrix
------------

The official Matrix website describes Matrix as "an open standard for decentralised persistent communication." Matrix is a communication platform that is based on decentralisation and federation, similar to Diaspora. More information is available on the Matrix website, and specifically in the General section of the FAQ[2].

Note that Matrix is only a standard. To actually use it, a full stack needs to be set up. In simplified terms, there needs to be a Matrix server and all users need to use a Matrix client. Details of the standard and reference implementations of the various parts are also available on the Matrix website.

Overview of our current setup
-----------------------------

We currently use matrix.org, the gratis Matrix service hosted by the people behind Matrix. We have the freedom to move to a different server at any time. Perhaps FSMK could host its own server in the near future. We will need to evaluate that idea at a later date.

We have created a public room (#fsmk-discuss) on the matrix.org server. This room is bridged to the #fsmk channel on Freenode IRC. The Freenode channel is in turn bridged to a Telegram supergroup. With this setup, any message posted on any of these platforms will be echoed to the others automatically.

The IRC bridge has been set up mainly to facilitate the Telegram bridge (there are currently no usable Matrix-Telegram bridges), but is also convenient for members who already actively use IRC.

The Telegram group exists to help members gradually make the switch. New members who do not use Matrix or IRC can be added to the Telegram group and eventually be asked to use Matrix. There are also a number of old members for whom switching to Matrix might not be possible or desirable right now. The Telegram bridge provides them a buffer.

Recommended clients
-------------------

As of this writing, we recommend Riot[3] as the Matrix client. It is currently the most feature-rich and most actively developed client and is available for a large number of platforms. Other free clients are listed on the Matrix website[4].

Conclusion
----------

Moving to Matrix is another attempt in FSMK's efforts to use a fully free and secure platform for communication. We have made this decision after much consideration. The choice of platform was debated keeping in mind the technical merits of platforms, user experience considerations, and other learnings from previous attempts to switch to a free platform. We have been using this Matrix setup for over two month now, and apart from small issues, we are largely happy with the results.

This migration also represents FSMK's desire to advance our technical infrastructure to aid smooth functioning of the organisation. Stay tuned for more announcements in this regard.

We urge you to check out Matrix and try it for yourself. Hop on to #fsmk-discuss:matrix.org and join us as we *discuss all the things!*

[1]: https://matrix.org/
[2]: https://matrix.org/docs/guides/faq.html#general
[3]: https://riot.im/
[4]: https://matrix.org/docs/projects/try-matrix-now.html#clients
